<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */
    protected $doc;

    /**
      * @param string $url Name of the skier logs XML file.
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
    }

    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {
        $clubs = array();
        // TODO: Implement the function retrieving club information
        #$xml = simplexml_load_file("tests/_data/SkierLogs.xml");
        # REF: http://php.net/manual/en/class.domxpath.php


        $xpath = new DOMXPath($this->doc);
        // This is a DOMNodeList:
        $results = $xpath->query("//Clubs/Club");
        #if (!is_null($elements)) {

        foreach ($results as $result) {
          $id     = $result->getAttribute('id');
          $name   = $result->getElementsByTagName('Name')->item(0)->nodeValue;
          $city   = $result->getElementsByTagName('City')->item(0)->nodeValue;
          $county = $result->getElementsByTagName('County')->item(0)->nodeValue;


          //$club = new Club($id, $name, $city,
                      //  $county);

          #  $club = new Club($id, $name, $city,$county);
          $club = new Club($id, $name, $city,$county);

//print_r($club);exit();
          $clubs[] = $club;
        #}

      }
        return $clubs;
    }

    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
    public function getSkiers()
    {
        $skiers = array();

        // TODO: Implement the function retrieving skier information,
        //       including affiliation history and logged yearly distances.
        $xpath = new DOMXPath($this->doc);
        $results = $xpath->query("/SkierLogs/Skiers/Skier");
        foreach ($results as $result)
        {
          $userName     = $result->getAttribute('userName');
          $firstName    = $result->getElementsByTagName('FirstName')->item(0)->nodeValue;
          $lastName     = $result->getElementsByTagName('LastName')->item(0)->nodeValue;
          $yearOfBirth  = $result->getElementsByTagName('YearOfBirth')->item(0)->nodeValue;

          $skier = new Skier($userName, $firstName, $lastName, $yearOfBirth);

          $aff = $xpath->query("/SkierLogs/Season[0]/Skiers/Skier");

# Cannot get this to work - it wont add the affiliation to the skier:

            for($i = 0; $i < $aff->length; $i++) {
            $j=0;
            if($userName == $aff->item($j)->getAttribute('userName')) {
            $clubId = $aff->item($j)->getAttribute('clubId');
            $a = new Affiliation($clubId, "Season[0]");
            $skier->addAffiliations($a);

            }
            $j++;
            }

          $aff2 = $xpath->query("/SkierLogs/Season[1]/Skiers/Skier");

            for($l = 0; $l < $aff2->length; $l++) {
            $k=0;
            if ($userName == $aff2->item($k)->getAttribute('userName')) {
            $clubId = $aff2->item($k)->getAttribute('clubId');
            $a2 = new Affiliation($clubId, "Season[1]");
            $skier->addAffiliations($a2);

            }
            $k++;
            }

            $skiers[] = $skier;
        }

# ITERATE THIS an assign to right skier:::
# As I cannot get to the right skier-object in the former code, I will not be able
# to add the distance right.
# I would calculate the distance by a query sorting out the @userName as I tried,
# but failed to find a solution for above.
# This type of query could be used:$dist = $xpath->query("sum(//Season[@fallYear="2015"]/Skiers[@clubId]/Skier[@userName="ande_andr"]/Log/Entry/Distance)"")

# Test 1, 2, 3 passing. Made a test on one-affiliation to may be find out how
# to solve the coding, but it did not help much. So the rest of the tests fails.
# I could have coded the distance part, but as I could not solve how to
# get the right skier-objects, I would not have managed to finish this either.




        return $skiers;
    }
}

?>
